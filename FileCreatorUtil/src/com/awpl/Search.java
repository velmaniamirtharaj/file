package com.awpl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.awpl.pojo.SubFile;
import com.awpl.pojo.ExistingFile;
import com.awpl.pojo.Folder;
import com.google.gson.internal.StringMap;

public class Search {

	public static void findFile(String fileName, LinkedHashMap<String, Object> directoryTree) {

		if (directoryTree == null) {
			throw new RuntimeException("Empty directory structure");
		}

		Folder rootFolder = new Folder();

		mapToPojo(directoryTree, rootFolder);

		LinkedList<ExistingFile> foundFilesList = new LinkedList<>();
		search(rootFolder, fileName, foundFilesList);

		Collections.sort(foundFilesList);

		if (foundFilesList.size() > 1) {
			if (foundFilesList.get(0).getLevel() == foundFilesList.get(1).getLevel()
					&& foundFilesList.get(0).getNoOfSiblings() == foundFilesList.get(1).getNoOfSiblings()) {
				System.out.println("Can't decide on the directory");
				return;
			}
		}

		if (!foundFilesList.isEmpty()) {
			ExistingFile found = foundFilesList.get(0);
			createDirectories(found.getPath());
		} else {
			System.out.println("Cannot create file - File not found");
		}
	}

	private static void mapToPojo(LinkedHashMap<String, Object> directoryTree, Folder rootFolder) {
		for (Map.Entry<String, Object> map : directoryTree.entrySet()) {

			rootFolder.setName(map.getKey());
			rootFolder.setLevel(1);
			@SuppressWarnings("unchecked")
			List<Object> list = (List<Object>) map.getValue();

			List<SubFile> files = new ArrayList<>();

			for (Object node : list) {
				if (node instanceof String) {
					SubFile file = new SubFile();
					file.setName(node.toString());
					file.setLevel(2);

					files.add(file);
					rootFolder.setFiles(files);
				} else {
					createPojo(map.getKey(), node, 1, rootFolder);
				}
			}
		}
	}

	public static void createDirectories(String path) {

		System.out.println("file created at " + path);

		File directory = new File(path);
		directory.getParentFile().mkdirs();

		try {
			FileWriter writer = new FileWriter(directory);
			BufferedWriter bw = new BufferedWriter(writer);
			bw.write("Hello World");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	@SuppressWarnings("rawtypes")
	public static void createPojo(Object key, Object value, int level, Folder folder) {

		if (value instanceof StringMap) {
			Iterator array = (((StringMap) value).entrySet()).iterator();

			List<Folder> subFolders = new ArrayList<>();
			++level;
			while (array.hasNext()) {

				Entry pair = (Entry) array.next();

				Folder subFolder = new Folder();
				subFolder.setName(pair.getKey().toString());
				subFolder.setLevel(level);
				subFolder.setParent(folder);

				createPojo(pair.getKey(), pair.getValue(), level, subFolder);

				subFolders.add(subFolder);
			}

			folder.getFolders().addAll(subFolders);

		} else if (value instanceof String) {

			SubFile leafFile = new SubFile();
			leafFile.setName(value.toString());
			folder.getFiles().add(leafFile);
			leafFile.setLevel(++level);
			return;

		} else if (value instanceof ArrayList) {
			List jsonArray = (List) value;

			for (Object oo : jsonArray) {
				createPojo(key, oo, level, folder);
			}
		}
	}

	public static void search(Folder folder, String fileName, LinkedList<ExistingFile> foundFilesList) {

		for (SubFile file : folder.getFiles()) {

			if (fileName.toLowerCase().equals(file.getName().toLowerCase())) {

				Folder current = folder;
				LinkedList<String> list = new LinkedList<>();
				while (current != null) {
					list.add(current.getName());
					current = current.getParent();
				}

				Collections.reverse(list);
				StringBuilder sb = new StringBuilder();
				for (String s : list) {
					sb.append(s + File.separator);
				}

				sb.append(fileName);
				ExistingFile foundFile = new ExistingFile();
				foundFile.setLevel(file.getLevel());
				foundFile.setPath(sb.toString());
				foundFile.setNoOfSiblings(folder.getFiles().size() + folder.getFolders().size());

				foundFilesList.add(foundFile);
			}
		}

		if (!folder.getFolders().isEmpty()) {
			for (Folder f : folder.getFolders()) {
				search(f, fileName, foundFilesList);
			}
		} else {
			return;
		}
	}
}

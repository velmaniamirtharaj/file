package com.awpl.pojo;

import java.util.ArrayList;
import java.util.List;

public class Folder {

	private String name;
	private int level;
	private List<SubFile> files = new ArrayList<>();
	private List<Folder> folders = new ArrayList<>();
	private Folder parent;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public List<SubFile> getFiles() {
		return files;
	}

	public void setFiles(List<SubFile> files) {
		this.files = files;
	}

	public List<Folder> getFolders() {
		return folders;
	}

	public void setFolders(List<Folder> folders) {
		this.folders = folders;
	}

	public Folder getParent() {
		return parent;
	}

	public void setParent(Folder parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		if (folders != null) {
			for (Folder ss : folders) {
				s.append(ss.getName() + "\\");
			}
		}
		return "Folder [name=" + name + ", level=" + level + ", files=" + files + ", folders=" + folders;
	}
}

package com.awpl.pojo;

public class ExistingFile implements Comparable<ExistingFile> {

	private String path;
	private Integer level;
	private Integer noOfSiblings;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getNoOfSiblings() {
		return noOfSiblings;
	}

	public void setNoOfSiblings(Integer noOfSiblings) {
		this.noOfSiblings = noOfSiblings;
	}

	@Override
	public String toString() {
		return "ExistingFile [path=" + path + ", level=" + level + ", noOfSiblings=" + noOfSiblings + "]";
	}

	@Override
	public int compareTo(ExistingFile o) {
		if (this.level < o.level) {
			return -1;
		} else if (this.level > o.level) {
			return 1;
		} else {
			if (this.noOfSiblings > o.noOfSiblings) {
				return -1;
			} else if (this.noOfSiblings > o.noOfSiblings) {
				return 1;
			} else {
				return 0;
			}
		}
	}
}
package com.awpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONException;

import com.awpl.pojo.Format;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

public class Reader {

	public static LinkedHashMap<String, Object> readFile(String location, Format format)
			throws FileNotFoundException, JSONException {

		if (format.name().toString().equals(Format.JSON.name().toString())) {
			return readJsonFileAsKeyValuePairs(location);
		} else {
			// check for other formats
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static LinkedHashMap<String, Object> readJsonFileAsKeyValuePairs(String location) {
		try {
			return (LinkedHashMap<String, Object>) (new Gson().fromJson(
					new JsonReader(new java.io.FileReader(new File(location))), new TypeToken<Map<String, Object>>() {
					}.getType()));
		} catch (FileNotFoundException e) {
			System.out.println(
					"JSON file not found - A json file named folder.json should be present in the current location");
			System.exit(0);
		} catch (JsonIOException e) {
			System.out.println("Invalid json - please have a valid json in folder.json");
			System.exit(0);
		}

		return null;
	}
}

package com.awpl;

import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Scanner;

import org.json.JSONException;

import com.awpl.pojo.Format;

public class Entry {

	private static final String LOCATION = "folder.json";

	public static void main(String[] args) throws JSONException, FileNotFoundException {

		LinkedHashMap<String, Object> directoryTree = Reader.readFile(LOCATION, Format.JSON);

		String fileName = readFileNameFromConsole();

		isFileExists(directoryTree, fileName);

		Search.findFile(fileName, directoryTree);
	}

	public static String readFileNameFromConsole() {
		String value = null;
		while (true) {
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter a file name");
			value = scanner.next();
			if (!value.contains(".")) {
				System.out.println("File name should include extension");
			} else {
				scanner.close();
				return value;
			}
		}
	}

	private static void isFileExists(LinkedHashMap<String, Object> directoryTree, String fileName) {

		String root = directoryTree.get("root").toString();

		if (root == null || root.isEmpty()) {
			throw new RuntimeException("Json is empty");
		}

		// Fast Fail - search for the file or directory name match in the entire
		// JSON string
		if (!root.toLowerCase().contains(fileName.toLowerCase())) {
			System.out.println("Cannot create file - file not found \n");
			throw new RuntimeException("Cannot create file - file not found in the json ");
		}
	}
}
